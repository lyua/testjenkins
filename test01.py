#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2020/3/29 8:35 下午
# @Author  : LiYuan

# 基础元素定位
from selenium import webdriver
from time import sleep


browser = webdriver.Chrome()
browser.get("https://www.baidu.com/")
sleep(2)
browser.get("https://cn.bing.com/")
sleep(1)

browser.quit()
